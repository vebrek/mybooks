var t_read = (function (){
    var read = false;
    return function () {
        read = !read;
        return read;}
})();


function toggle_read() {
    if(t_read()) $("#read").removeClass("btn-danger")
                           .addClass("btn-success")
                           .val("true");
    else         $("#read").removeClass("btn-success")
                           .addClass("btn-danger")
                           .val("false");

}

$(document).ready(function(){
    load_books();
});


function load_books(){
    $.ajax({
        url: 'php/save_book.php',
        type: 'GET',
        data: {action: 'print'},

        success: function(data){
            var header = "<tr><th>Bok</th><th>Forfatter</th><th>År</th></tr>"
            $('#book_table').html(header + data);
        }
    });
}
