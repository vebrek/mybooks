function save_book(){
    var author = $("#author").val();
    $.ajax({
        url: 'php/save_book.php',
        type: 'GET',
        data: {action: 'insert',
               title: $('#title').val(),
               author: $('#author').val(),
               year: $('#year').val(),
               isbn: $('#isbn').val(),
               read: $('#read').val()},

        success: function(data){
            $('#success_store_book').html(data);
        }
    });
}
